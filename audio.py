import pandas as pd
from scipy.io import wavfile
import os
import fnmatch
import numpy as np
NUM_FRAMES = 600


def get_wave_file_paths(rootDir="/Users/ARZU/Documents/Project576/databse_videos/"):
    fname_dict = {}
    for dirName, subdirList, fileList in os.walk(rootDir):
        # print('Found directory: %s' % dirName)
        for fname in fileList:
            file_name = os.path.join(dirName, fname)
            if '.wav' in fname:
                fname_dict[file_name] = find('*.rgb', dirName)

    return fname_dict


def find(pattern, path):
    result = []
    for root, dirs, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern):
                result.append(os.path.join(root, name))
    return result


def open_wave_file(filepath="/Users/ARZU/Downloads/query/second/second.wav"):
    """  loads wav file """
    fs, data = wavfile.read(filepath)
    df = pd.DataFrame(data, columns=["left", "right"])
    return df


def norm(x):
    """
    >>> b=[3,4]
    >>> norm(b)
    >>> norm(np.array(b))
    5.0
    """
    return np.sqrt((x**2).sum())


def correlation(x, y):
    x = np.array(x)
    y = np.array(y)
    normx = norm(x)
    normy = norm(y)
    return x.dot(y) / normx / normy


def correlation_with_q(v):
    return correlation(correlation_with_q.q, v)


correlation_with_q.q = []


def query_audio(q):
    correlation_with_q.q = q
    df.left_std.rolling(len(q)).apply(correlation_with_q).idxmax()
    df.iloc[1140:1146]


def main():
    files_to_process = {}
    files_to_process = get_wave_file_paths()
    audio_frames = []
    for audio_file_name, image_file_names in files_to_process.items():
        df = open_wave_file(audio_file_name)
        sample_size = len(df.index)
        print(sample_size)
        print(len(image_file_names))
        samples_per_frame = int(sample_size / len(image_file_names))
        print(samples_per_frame)
        for i, image_file_name in enumerate(image_file_names):
            aa = df.left[i * samples_per_frame:(i + 1) * samples_per_frame - 1].std()
            bb = df.right[i * samples_per_frame:(i + 1) * samples_per_frame - 1].std()
            audio_frames.append([audio_file_name, image_file_name, i, aa, bb])
    return pd.DataFrame(audio_frames, columns="audio_file image_file_name  frame_number left_std right_std".split())


if __name__ == "__main__":
    pass
    #audio_frames = main()
